import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { ShoppingCartService } from '../services/shoppingCart.service';
import { LoginServices } from '../services/Login.services';
import { LoginCredentials } from '../Models/requestObject/Login.Models';
import { CommonService } from '../services/common.service';
import { debounce } from 'rxjs/internal/operators/debounce';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  constructor(
    private route: Router,
    public shoppingCartService: ShoppingCartService,
    private loginService: LoginServices,
    public commonService: CommonService,
    private router: Router
  ) {
  }
  containerInside: boolean;
  userName: string;
  noOfProd: any;
  isloged: boolean;
  @Input() showNav: boolean;
  ngOnInit() {
    // debugger
    if (localStorage.myCart !== undefined) {
      const myCart = JSON.parse(localStorage.myCart);
      this.shoppingCartService.itemsQuntity = myCart.length;
    }
    this.shoppingCartService.emitter.subscribe(x => {
      if (x) {
        this.shoppingCartService.itemsQuntity = 0
      }
    });
  }
  logout() {
    this.commonService.showLoading = true;
    this.loginService.Logout().subscribe(res => {
      localStorage.clear();
      this.shoppingCartService.myCart = [];
      this.router.navigate(['/login']);
      this.shoppingCartService.itemsQuntity = 0
      this.commonService.showLoading = false;
    });
  }
  openNav() {
    document.getElementById("mySidenav").style.width = "280px";
    document.getElementById("overlay3").style.width = "100%";
    document.getElementById("overlay3").style.opacity = "0.6";
  }
  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("overlay3").style.width = "0";
    document.getElementById("overlay3").style.opacity = "0";
  }
}
