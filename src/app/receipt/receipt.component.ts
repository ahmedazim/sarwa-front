import { Component, OnInit } from '@angular/core';
import { PrintingService } from '../services/printing.service';

@Component({
  selector: 'app-receipt',
  templateUrl: './receipt.component.html',
  styleUrls: ['./receipt.component.scss']
})
export class ReceiptComponent implements OnInit {

  clientId: string = localStorage.clientId;
  constructor(public printingService: PrintingService) { }

  ngOnInit() {
  }

}
