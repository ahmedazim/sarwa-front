import { Component, OnInit } from '@angular/core';
import { LoginCredentials } from '../Models/requestObject/login.models';
import { LoginServices } from '../services/Login.services';
import { Router } from '@angular/router';
import { LoginResponse } from '../Models/loginResponse';
import { CommonService } from '../services/common.service';
import { ShoppingCartService } from '../services/shoppingCart.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loginCred: LoginCredentials;
  error = false;

  constructor(private loginService: LoginServices, private shoppingCartService: ShoppingCartService,
    private route: Router, private commonService: CommonService) {
    this.loginCred = new LoginCredentials();
  }

  submitLogin(formData) {

    if (formData.userName !== undefined && formData.password !== undefined) {
      this.loginCred.authUsername = formData.userName;
      this.loginCred.authPassword = formData.password;
      this.commonService.showLoading = true;

      this.loginService.Login(this.loginCred).subscribe((data: any) => {
        // alert(data["SessionId"]);

        if (data.data.sessionId !== null || data.data.sessionId !== undefined) {
          localStorage.setItem('sessionId', data.data.sessionId);
          const userData = window.btoa(this.loginCred.authUsername + ':' + this.loginCred.authPassword);
          localStorage.setItem('currentUser', userData);
          localStorage.username = this.loginCred.authUsername;
          this.commonService.username = this.loginCred.authUsername;
          this.shoppingCartService.removeShoppingCart();
          this.route.navigate(['customer']);
        } else {
          this.error = true;
        }
        this.commonService.showLoading = false;
      }, error => {
        this.error = true;
        this.commonService.showLoading = false;

      });
    }
  }

}


// (function() {
//   'use strict';
//   window.addEventListener('load', function() {
//     // Fetch all the forms we want to apply custom Bootstrap validation styles to
//     var forms = document.getElementsByClassName('needs-validation');
//     // Loop over them and prevent submission
//     var validation = Array.prototype.filter.call(forms, function(form) {
//       form.addEventListener('submit', function(event) {
//         if (form.checkValidity() === false) {
//           event.preventDefault();
//           event.stopPropagation();
//         }
//         form.classList.add('was-validated');
//       }, false);
//     });
//   }, false);
// })();
