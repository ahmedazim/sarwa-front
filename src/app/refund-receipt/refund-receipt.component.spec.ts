import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefundReceiptComponent } from './refund-receipt.component';

describe('RefundReceiptComponent', () => {
  let component: RefundReceiptComponent;
  let fixture: ComponentFixture<RefundReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefundReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
