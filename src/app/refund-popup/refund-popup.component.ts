import { Component, OnInit } from '@angular/core';
import { SmsService } from '../services/sms.service';
import { InvoiceSummaryService } from '../services/invoice-summary.service';
import { RefundItem } from '../Models/requestObject/refund-item';
import { CommonService } from '../services/common.service';
import { Router } from '@angular/router';
import { CheckOutServices } from '../services/checkOut.service';

@Component({
  selector: 'app-refund-popup',
  templateUrl: './refund-popup.component.html',
  styleUrls: ['./refund-popup.component.scss']
})
export class RefundPopupComponent implements OnInit {
  mobileNo: string;
  viewMobileNo: string;
  refundedItems: RefundItem;
  enableConfirmBtn = false;
  otpErrorMessage: string;
  errorMessage: string;
  successMessage: string;
  clientId = '';
  constructor(private smsService: SmsService,
    private router: Router,
    private checkOutService: CheckOutServices,
    private invoiceService: InvoiceSummaryService,
    private commonService: CommonService) {
    this.refundedItems = this.commonService.refundedListItem;
  }

  ngOnInit() {
    this.clientId = localStorage.getItem('clientId');
    this.sendOtpToClient(this.clientId);
    this.getMobileNumber();
  }
  getMobileNumber() {
    this.errorMessage = '';
    this.commonService.showLoading = true;
    const clientId = localStorage.getItem('clientId');
    this.checkOutService.getMobileNumber(clientId).subscribe(
      (res: any) => {
        this.mobileNo = res.mobile;
        this.commonService.showLoading = false;
      },
      err => {
        this.commonService.showLoading = false;
      }
    );
  }
  sendOtpToClient(clientID) {
    this.smsService.sendOtp(clientID, 2, 0).subscribe(c => {

      console.log('success');
      console.log(c);


    }, error => {
      console.log('error');
    });
  }
  // verfiyOtp(otp) {
  //   this.smsService.verifyOtp(this.mobileNo, otp).subscribe(res => {
  //     this.enableConfirmBtn = true;
  //     this.confirmRefund();
  //   }, error => {
  //     this.otpErrorMessage = '	كود التأكيد غير صحيح يرجى إعادة المحاولة';
  //   });
  // }
  confirmRefund(otp) {
    this.refundedItems.otp = otp;
    // this.refundedItems.mobileNo = this.mobileNo;
    this.invoiceService.RefundItem(this.refundedItems).subscribe((res: any) => {
      this.router.navigateByUrl(`check-out/success/refund/${res.refundNo}`);
      this.successMessage = 'عملية ناجحة';
    }, err => {
      this.errorMessage = 'يتعذر الإتصال بالنظام، يرجى إعادة المحاولة، في حالة إستمرار المشكلة يرجى الإتصال بمدير العلاقات التجارية المسئول';
    });
  }
}
