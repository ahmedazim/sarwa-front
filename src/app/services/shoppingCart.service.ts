import { Injectable, Output, EventEmitter } from "@angular/core";
import { JsonPipe } from '@angular/common';

@Injectable()
export class ShoppingCartService {
  public myCart = [];
  public itemsQuntity = 0;
  public totalsForInvoice = { price: 0, downPayment: 0 };
  @Output() emitter: EventEmitter<boolean> = new EventEmitter();
  @Output() Addemitter: EventEmitter<any> = new EventEmitter();

  constructor() {
    this.itemsQuntity = this.getItemsQuntity();
    if (localStorage.myCart !== undefined) {
      this.myCart = JSON.parse(localStorage.myCart);
      if (this.myCart.length > 0) {
        this.totalsForInvoice.price = this.myCart.map(x => Number(x.price)).reduce((a, b) => a + b);
        this.totalsForInvoice.downPayment = this.myCart.map(x => Number(x.downPayment)).reduce((a, b) => a + b);
      }
    }

  }
  fireAddProduct(products) {   
    this.Addemitter.emit(products);
  }
  fireDeleteEvent() {
    this.emitter.emit(true);
  }
  
  deepIndexOf(arr, obj) {
    return arr.findIndex(function (cur) {
      return Object.keys(obj).every(function (key) {
        return obj[key] === cur[key];
      });
    });
  }
  addToCart(product, quntity) {
    // const filterdItems = this.myCart.filter(item => item.itemId === product.itemId);
    // if (filterdItems.length === 0) {
    //   product.quantity = 1;
    for (let i = 0; i < quntity; i++) {
      this.myCart.push(product);
      this.Addemitter.emit(product);
    }
    // } else {
    //   filterdItems[0].quantity += 1;
    // }
    localStorage.removeItem('myCart');
    localStorage.myCart = JSON.stringify(this.myCart);
    this.itemsQuntity = this.getItemsQuntity();
    this.totalsForInvoice = this.getTotals();
  }

  removeShoppingCart() {
    localStorage.removeItem('myCart');
    this.itemsQuntity = 0;
    this.myCart = [];
    this.totalsForInvoice = this.getTotals();

  }
  addToCartWithQuntity(product, quntity: number) {
    const filterdItems = this.myCart.filter(item => item.itemId === product.itemId);

    filterdItems[0].quantity = quntity;
    localStorage.myCart = JSON.stringify(this.myCart);
    this.itemsQuntity = this.getItemsQuntity();
    this.totalsForInvoice = this.getTotals();
  }
  minusQuntity(prod) {
    const filterdItems = this.myCart.filter(item => item.itemId === prod.itemId);
    if (filterdItems.length >= 1) {
      if (filterdItems[0].quantity === 1) {
        this.removeFromCart(prod);
      }
      filterdItems[0].quantity -= 1;
      localStorage.myCart = JSON.stringify(this.myCart);
      this.itemsQuntity = this.getItemsQuntity();
      this.totalsForInvoice = this.getTotals();
    }
  }
  removeFromCart(product) {
    this.myCart.splice(product, 1);
    localStorage.removeItem('myCart');
    localStorage.myCart = JSON.stringify(this.myCart);
    this.itemsQuntity = this.getItemsQuntity();
    this.totalsForInvoice = this.getTotals();
  }
  getItemsQuntity() {
    const quntity = this.myCart.length;
    // this.myCart.forEach(item => {
    //   quntity += item.quantity;
    // });
    return quntity;
  }
  getQuntity(prod) {
    if (this.myCart.find(c => c.itemId === prod.itemId) !== undefined) {
      return this.myCart.find(c => c.itemId === prod.itemId).quantity > 0
        ? this.myCart.find(c => c.itemId === prod.itemId).quantity
        : 0;
    }
    return 0;
  }
  getActQuntity(prod) {
    if (this.myCart.find(c => c.Id === prod.Id) !== undefined) {
      return this.myCart.find(c => c.Id === prod.Id).Quntity > 0
        ? this.myCart.find(c => c.Id === prod.Id).Quntity
        : 0;
    }
    return 0;
  }
  calcCartQuntity() {
    this.itemsQuntity = this.getItemsQuntity();
  }
  getTotals() {
    const totalsObj = { price: 0, downPayment: 0 };
    this.myCart.forEach(item => {
      totalsObj.price += Number(item.price);
      totalsObj.downPayment += Number(item.downPayment);
    });
    return totalsObj;
  }



}
