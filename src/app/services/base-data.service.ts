import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseDataService {
  APIEndpoint = environment.APIEndpoint;

  public sessionId: string;
  constructor() {
    this.sessionId = this.getSessionId();
   }
   getSessionId(): string {
    return this.sessionId = localStorage.getItem('sessionId');
  }
   getAuthHeader() {
    return  {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Basic ${localStorage.getItem('currentUser')}`
      })};
  }
}
