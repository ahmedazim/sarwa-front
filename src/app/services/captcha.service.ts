import { Injectable } from '@angular/core';
import { BaseDataService } from './base-data.service';
import { HttpClient } from '@angular/common/http';
import { CaptchaModel } from '../Models/requestObject/captcha.model';

@Injectable()
export class CaptchaServices extends BaseDataService {
  constructor(private http: HttpClient) {
    super();
  }

  send(data: CaptchaModel) {
    return this.http.post('http://localhost:2489/api/Captcha', data);
  }
}
