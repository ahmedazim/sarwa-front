import { Injectable } from '@angular/core';
import { Base } from '../Models/requestObject/base.model';
import { HttpClient } from '@angular/common/http';
import { BaseDataService } from './base-data.service';
import { RefundItem } from '../Models/requestObject/refund-item';

@Injectable({
  providedIn: 'root'
})
export class InvoiceSummaryService extends BaseDataService {

  constructor(private http: HttpClient) {
    super();
  }
  getInvoiceHistory(clientId: string) {
    return this.http.get(this.APIEndpoint + 'invoice/GetInvoicesByClientId?sessionId='
      + this.getSessionId() + '&clientId=' + clientId, this.getAuthHeader());
  }
  getInvoiceDetails(clientId: string, invoiceNumber: number) {
    return this.http.get(this.APIEndpoint + 'invoice/GetInvoicesDetails?sessionId='
      + this.getSessionId() + '&clientId=' + clientId + '&invoiceNumber=' + invoiceNumber, this.getAuthHeader());
  }
  getRefundeDetails(clientId: string, refundNumber: number) {
    return this.http.get(this.APIEndpoint + 'invoice/GetRefundDetails?sessionId='
      + this.getSessionId() + '&clientId=' + clientId + '&refundNumber=' + refundNumber, this.getAuthHeader());
  }
  RefundItem(refundItem: RefundItem) {
    return this.http.post(this.APIEndpoint + 'invoice/RefundItem?sessionId='
      + this.getSessionId(), refundItem, this.getAuthHeader());
  }
}
