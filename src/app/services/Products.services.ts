import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CategoryRes } from '../Models/responseObject/category.response.model';
import { SubCategoryRes } from '../Models/responseObject/subCategory.response.model';
import { ProductRes } from '../Models/responseObject/product.response.model';
import { map } from 'rxjs/operators';
import { BaseDataService } from './base-data.service';

@Injectable({
  providedIn: 'root'
})
export class ProductServices extends BaseDataService {
  private productList: ProductRes[];

  constructor(private http: HttpClient) {
    super();
  }

  getCategories() {
    return this.http
      .get(this.APIEndpoint + 'Categories?sessionId=' + this.getSessionId(), this.getAuthHeader())
      .pipe(
        map(data => {
          return data as Array<CategoryRes>;
        })
      );
  }

  getSubCategories(categoryId: number) {
    return this.http
      .get(
        this.APIEndpoint +
          'SubCategories?sessionID=' +
          this.getSessionId() +
          '&categoryId=' +
           categoryId,
           this.getAuthHeader()
      )
      .pipe(
        map(data => {
          return data as Array<SubCategoryRes>;
        })
      );
  }

  getProducts(subCategoryId: number) {
    return this.http
      .get(
        this.APIEndpoint +
          'Items?sessionId=' +
          this.getSessionId() +
          '&subCategoryId=' +
          subCategoryId,
          this.getAuthHeader()
      )
      .pipe(
        map(data => {
          return data as Array<ProductRes>;
        })
      );
  }

  setProductList(prodListObj: ProductRes []) {
    this.productList = prodListObj;
  }

  getProductList() {
    return this.productList;
  }
}
