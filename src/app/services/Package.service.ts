import { Injectable, Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseDataService } from './base-data.service';

@Injectable()
export class PackageService extends BaseDataService {

    constructor(private http: HttpClient) {
      super();
    }
   getPackage(clientId: string) {
       return this.http.get(this.APIEndpoint + 'Package?sessionId=' + this.getSessionId() + '&clientId=' + clientId,
        this.getAuthHeader());
    }


}
