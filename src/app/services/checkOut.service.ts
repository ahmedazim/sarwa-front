import { Injectable, Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseDataService } from './base-data.service';
import { Invoice } from '../Models/requestObject/Invoice.model';


@Injectable()
export class CheckOutServices extends BaseDataService {
    constructor(private http: HttpClient) {
      super();
    }
     getMobileNumber(clientId: string) {
       return this.http.get(this.APIEndpoint + 'ClientMobile?sessionId=' + this.getSessionId() +
        '&clientId=' + clientId, this.getAuthHeader());
    }
    submitInvoice(invoiceObj: Invoice) {
      return this.http.post(this.APIEndpoint + 'invoice/submitInvoice?sessionId=' + this.getSessionId(), invoiceObj, this.getAuthHeader());
    }

}
