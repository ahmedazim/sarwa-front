import { Injectable } from '@angular/core';
import { RefundItem } from '../Models/requestObject/refund-item';
import { toBase64String } from '@angular/compiler/src/output/source_map';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  public showNav = false;
  public showLoading = false;
  public refundedListItem: RefundItem;
  public username: string;
  constructor() {
    this.refundedListItem = (localStorage.refundedItems ?
      JSON.parse(localStorage.refundedItems) : this.refundedListItem );
    this.username = localStorage.username || '';
  }

  isUserLogged() {
    return !!localStorage.getItem('username');
  }
}
