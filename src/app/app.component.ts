import { Component, HostListener } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { CommonService } from './services/common.service';
import { LoginServices } from './services/Login.services';
import { BnNgIdleService } from 'bn-ng-idle';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  constructor(private router: Router, public commonService: CommonService, private loginService: LoginServices,
              private bnIdle: BnNgIdleService) {
    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        // tslint:disable-next-line:curly
        if (event.url === '/login' || event.url === '/')
          commonService.showNav = false;
        // tslint:disable-next-line:curly
        else
          commonService.showNav = true;
      }
    });

    this.bnIdle.startWatching(300).subscribe((res) => {
      if (res) {
        this.loginService.Logout();
        localStorage.clear();
        this.router.navigate(['/login']);
      }
    });
   }
  @HostListener('window:beforeunload', ['$event'])
  public beforeunloadHandler($event) {
    this.loginService.Logout();
    localStorage.clear();
  }
}



