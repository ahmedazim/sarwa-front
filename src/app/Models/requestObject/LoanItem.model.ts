export class LoanItem {
    itemId: number;
    code: string;
    price: number;
    downPayment: number;
    tenor: number;
    isBonus: boolean;
    quantity: number;
    nameAr: string;
    name: string;
    installment: number;
    SubName: string;
    categoryId:number;
}
