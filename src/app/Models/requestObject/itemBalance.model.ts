import { Base } from './base.model';

export class ItemBalance extends Base {

  constructor( public loanAmount: number, public tenor: number) {
    super();
  }
}
