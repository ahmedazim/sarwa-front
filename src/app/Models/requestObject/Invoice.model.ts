import {  LoanItem } from './LoanItem.model';
import { Base } from './base.model';

export class Invoice extends Base {
    price = 0;
    downPayment = 0 ;
    items: LoanItem[];
    mobileNo: string;
    otp: string;
}
