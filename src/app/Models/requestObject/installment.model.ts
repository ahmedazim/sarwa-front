import { Base } from './base.model';

export class Installment extends Base {
  loanAmount: number;
  tenor: number;
}
