import { Base } from './base.model';
import { InstallmentSeq } from './installmentSeq';

export class RefundItem extends Base {
  invoiceNo: string;
  items: InstallmentSeq[] = [];
  otp: string;
  mobileNo: string;
}
