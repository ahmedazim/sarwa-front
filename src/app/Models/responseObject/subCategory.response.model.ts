export class SubCategoryRes{
    ID: number;
    Code: string;
    Name: string;
    NameAr: string;
}