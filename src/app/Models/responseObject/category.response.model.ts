export class CategoryRes{
    Tenor: string;
    ID: number;
    Code: number;
    Name: string;
    NameAr: string;
}