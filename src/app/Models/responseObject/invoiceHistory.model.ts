export class InvoiceHistory {
   price: number;
   downPayment: number;
   invoiceNo: number;
   date: Date;
   showDetails: boolean;
}
