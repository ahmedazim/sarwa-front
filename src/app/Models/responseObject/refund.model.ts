import { InvoiceItems } from './invoiceItems.model';

export class Refund {
    invoiceNo: string;
    invoiceDate: string;
    refundNo: string;
    refundDate: string;
    items: InvoiceItems [];
    client:string;
    merchant: string;
    merchantBranch: string;

  }
