export class PackageRes{
    id: number;
    code: number;
    name: string;
    interestRate: number;
    minDuration: number;
    maxDuration:number;
    minLoan: number;
}