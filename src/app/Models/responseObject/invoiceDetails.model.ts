import { InvoiceItems } from './invoiceItems.model';

export class InvoiceDetails {
  price: number;
  downPayment: number;
  invoiceNo: string;
  date: Date;
  client: string;
  merchant: string;
  merchantBranch: string;
  items: InvoiceItems [];
}
