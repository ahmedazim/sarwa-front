import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PrintingService } from '../services/printing.service';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {
  invoiceNo: number;
  refund: string;
  constructor(private route: ActivatedRoute,
              private printService: PrintingService
  ) {
    route.params.subscribe(c => {
      this.invoiceNo = c.invoiceNo;
      this.refund = c.refund;
    });
  }

  ngOnInit() {
  }
  print() {
    // let originalContents = document.body.innerHTML;
    // document.body.innerHTML = printedArea;
    // window.print();
    // document.body.innerHTML = originalContents;
    if (this.refund === 'refund') {
      this.printService.printRefundDocument(this.invoiceNo);
    } else {
      this.printService
        .printDocument(this.invoiceNo);
    }
  }
}
