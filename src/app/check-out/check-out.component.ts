import { Component, ViewChild, OnInit } from '@angular/core';
import { ShoppingCartService } from '../services/shoppingCart.service';
import { CheckOutServices } from '../services/checkOut.service';
import { Invoice } from '../Models/requestObject/Invoice.model';
import { CommonService } from '../services/common.service';
import { SmsService } from '../services/sms.service';
import { Router } from '@angular/router';
import { stringify } from 'querystring';
import { PrintingService } from '../services/printing.service';
import { LoanItem } from 'src/app/Models/requestObject/LoanItem.model';
import { CaptchaComponent } from 'angular-captcha';
import { CaptchaServices } from '../services/captcha.Service';
import { CaptchaModel } from '../Models/requestObject/captcha.model';
@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.scss']
})
export class CheckOutComponent implements OnInit {
  private invoiceNo: number;
  public mobileNoReq: string;
  public clientId = '';
  cartItems: LoanItem[];
  public errorMessage: string;
  otpErrorMessage: string;
  // @ViewChild(CaptchaComponent, { static: true }) captchaComponent: CaptchaComponent;
  constructor(
    public shoppingService: ShoppingCartService,
    private checkOutService: CheckOutServices,
    private commonService: CommonService,
    private smsSerivce: SmsService,
    private router: Router,
    private printingService: PrintingService,
    private captchaServices: CaptchaServices
  ) {
    this.otpErrorMessage = '';
    this.getMobileNumber();
    this.clientId = localStorage.getItem('clientId');
    this.sendOTP(this.clientId);
    if (localStorage.myCart !== undefined) {
      this.cartItems = JSON.parse(localStorage.myCart);
    } else {
      this.cartItems = [];
    }
  }
  ngOnInit(): void {
    // set the captchaEndpoint property to point to
    // the captcha endpoint path on your app's backend
    // this.captchaComponent.captchaEndpoint = 'http://localhost:2489/simple-captcha-endpoint.ashx';
  }
  // varifyOTP(data) {
  //   this.errorMessage = "";
  //   this.otpErrorMessage = "";
  //   this.smsSerivce.verifyOtp(this.mobileNoReq, data.otpCode).subscribe(
  //     (res: any) => {
  //       this.submitInvoice();
  //     },
  //     err => {
  //       this.otpErrorMessage = 'كود التأكيد غير صحيح يرجى إعادة المحاولة.';
  //     }
  //   );
  // }
  // validate() {

  //   // get the user-entered captcha code value to be validated at the backend side
  //   const userEnteredCaptchaCode = this.captchaComponent.userEnteredCaptchaCode;

  //   // get the id of a captcha instance that the user tried to solve
  //   const captchaId = this.captchaComponent.captchaId;

  //   const postData = new CaptchaModel();
  //   postData.captchaId = captchaId;
  //   postData.userEnteredCaptchaCode = userEnteredCaptchaCode;
  //   debugger;
  //   // post the captcha data to the backend
  //   this.captchaServices.send(postData)
  //     .subscribe(
  //       response => {
  //         console.log(response);
  //         // if (response.success === false) {
  //         //   // captcha validation failed; reload image
  //         //   this.captchaComponent.reloadImage();
  //         //   // TODO: maybe display an error message, too
  //         // } else {
  //         //   // TODO: captcha validation succeeded; proceed with the workflow
  //         // }
  //       });
  // }
  getMobileNumber() {
    this.errorMessage = '';
    this.commonService.showLoading = true;
    const clientId = localStorage.getItem('clientId');
    this.checkOutService.getMobileNumber(clientId).subscribe(
      (res: any) => {
        this.mobileNoReq = res.mobile;

        this.commonService.showLoading = false;
      },
      err => {
        this.commonService.showLoading = false;
        // tslint:disable-next-line:max-line-length
        this.errorMessage = 'يتعذر الإتصال بالنظام، يرجى إعادة المحاولة، في حالة إستمرار المشكلة يرجى الإتصال بمدير العلاقات التجارية المسئول.';
      }
    );
  }
  sendOTP(clientID) {
    const totalAmount = this.shoppingService.getTotals().price - this.shoppingService.getTotals().downPayment;
    this.smsSerivce.sendOtp(clientID, 1, totalAmount).subscribe(
      (res: any) => {
        console.log(res);
      },
      err => {
        console.log(err);
      }
    );
  }
  submitInvoice(data) {
    this.commonService.showLoading = true;
    const invoice = new Invoice();
    invoice.items = this.shoppingService.myCart;
    invoice.price = this.shoppingService.getTotals().price;
    invoice.downPayment = this.shoppingService.getTotals().downPayment;
    invoice.otp = data.otpCode;
    // invoice.mobileNo = this.mobileNoReq;
    this.checkOutService.submitInvoice(invoice).subscribe(
      (res: any) => {
        this.invoiceNo = res.invoiceNo;
        this.shoppingService.myCart = [];
        localStorage.myCart = JSON.stringify(this.shoppingService.myCart);
        this.shoppingService.itemsQuntity = 0;
        this.commonService.showLoading = false;
        this.router.navigateByUrl(`check-out/success/invoice/${this.invoiceNo}`);
      },
      err => {
        this.commonService.showLoading = false;
        this.errorMessage = err.error.errorMessage;
        // else
        //   this.errorMessage = 'يتعذر الإتصال بالنظام، يرجى إعادة المحاولة، في حالة إستمرار المشكلة يرجى الإتصال بمدير العلاقات التجارية المسئول.';

        console.log(err);
      }
    );
  }

}
